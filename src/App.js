import React, { Component } from 'react';
import './App.css';

import {
  BrowserRouter as Router,
  Route,
  //Link
} from 'react-router-dom';
import ViewComponent from './ViewComponent.js';

class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/about" component={ViewComponent}/>
      </Router>
    );
  }
}

export default App;
