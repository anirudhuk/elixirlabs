import React from 'react';
import { Tab, InputGroup, Row, Col, Nav, NavItem, Image } from 'react-bootstrap';
import classnames from 'classnames';
import MyProfile from './MyProfile.js';
import myImage from './assets/thumbnail.png';
import myProfileImage from './assets/jobs-jpg.jpg';
import myProfileImage2 from './assets/zuck.jpg';
//import './App.css';

class BootstrapComp extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '2'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      
          <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row className="">
              <Col sm={12}>
                <Nav bsStyle="tabs" justified>
                  <NavItem eventKey="first">
                    <Col>
                      <Image src={myProfileImage} circle />
                    </Col>
                  </NavItem>
                  <NavItem eventKey="second">
                    <Col>
                      <Image src={myProfileImage2} style={{"height":"59px"}} circle />
                    </Col>
                  </NavItem>
                  <NavItem eventKey="third">
                    <Col>
                      <Image src={myProfileImage} circle />
                    </Col>
                  </NavItem>
                  <NavItem eventKey="fourth">
                    <Col>
                      <Image src={myProfileImage2} style={{"height":"59px"}} circle />
                    </Col>
                  </NavItem>
                </Nav>
              </Col>
              <Col sm={12}>
                <Tab.Content animation>
                  <Tab.Pane eventKey="first">
                    <MyProfile name="akshay"/>
                  </Tab.Pane>
                  <Tab.Pane eventKey="second">
                    <MyProfile name="anirudh"/>
                  </Tab.Pane>
                  <Tab.Pane eventKey="third">
                    <MyProfile name="arihant"/>
                  </Tab.Pane>
                  <Tab.Pane eventKey="fourth">
                    <MyProfile name="manoj"/>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
      
    );
  }
}

export default BootstrapComp;