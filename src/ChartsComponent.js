import React from 'react';
import { Button, Row, Col, FormControl, Jumbotron } from 'react-bootstrap';
import classnames from 'classnames';
import myData from './data/profileData.json';
var _ = require('lodash');
var DoughnutChart = require("react-chartjs").Doughnut;

const chartOptions = {
  responsive : true
};

class ChartsComponent extends React.Component {

	constructor(props) {
	    super(props);

	    this.state = {
	      domain1: [],
	      domain2: []
	    };
  	}
  	 
  	componentDidMount() {
      const profileName = this.props.name;
      const profileDetails = myData[profileName] 
      this.setState({ domain: profileDetails.domainExpertise[this.props.currentDomain]});
  	}

	render() {
		return(
			<DoughnutChart data={this.state.domain} options={chartOptions} />
		);
	}
}

export default ChartsComponent;