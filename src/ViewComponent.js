import React, { Component } from 'react';
import { Label } from 'react-bootstrap';
import BootstrapComp from './BootstrapComp.js';

class ViewComponent extends Component {
clickCounter = 0;

  constructor(props) {
      super(props);
    
      this.state = {
         header: "Header from state...",
         content: "Content from state...",
         buttonLabel: 'Increment Count'
      }
   }

  render() {
    return (
      
     <div className="App">
        <div className="App-header">
          <h1>Welcome to our Portfolio</h1>
        </div>
      <h2><Label bsStyle="primary">Contributors</Label> </h2>  
      <BootstrapComp />
      </div>
    );
  }
}

export default ViewComponent;
