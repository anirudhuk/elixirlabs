import React from 'react';
import { Button, InputGroup, Input, Row, Col, FormControl, Jumbotron, Image, Thumbnail } from 'react-bootstrap';
import classnames from 'classnames';
import myData from './data/profileData.json';
import myImage from './assets/thumbnail.png';
import myProfileImage from './assets/image-test.jpg';
import ChartsComponent from './ChartsComponent.js';
var _ = require('lodash');
var DoughnutChart = require("react-chartjs").Doughnut;

const data = [
            {color: "red", label: "test1", value: 75},
            {color: "white", label: "test2", value: 25}
            ];

const chartOptions = {
  responsive : true
};

var style = {
  width:'200px'
};

class MyProfile extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      linkedInUrl: '',
      gitHubUrl: '',
      experiencedDomain: '',
      profileName: '',
      summary: '',
      domain1: [],
      domain2: []
    };
  }

  componentDidMount() {
      //console.log(myData);
      const profileName = this.props.name;
      const profileDetails = myData[profileName] 
      this.setState({linkedInUrl: profileDetails.linkedIn , gitHubUrl: profileDetails.gitHub, experiencedDomain: profileDetails.experiencedDomain, 
        profileName: this.props.name, summary: profileDetails.summary, domain1: profileDetails.domainExpertise.domain1, domain2: profileDetails.domainExpertise.domain2 });
  }

  routeToLinkedIn() {
    const profileDetails = myData[this.state.profileName]
    window.open(profileDetails.linkedIn,'_blank');
  }

  routeToGitHub() {
    const profileDetails = myData[this.state.profileName]
    window.open(profileDetails.gitHub,'_blank');
  }
/*           <InputGroup>
              <InputGroup.Button>
                <Button bsStyle="primary">LinkedIn</Button>
              </InputGroup.Button>
              <FormControl placeholder="LinkedIn" type="text" value={this.state.linkedInUrl} readOnly/>
            </InputGroup>  
            <br/>
            <InputGroup>
              <InputGroup.Button>
                <Button bsStyle="danger">GitHub</Button>
              </InputGroup.Button>
              <FormControl placeholder="GitHub" type="text" value={this.state.gitHubUrl} readOnly/>
            </InputGroup>
*/
//<DoughnutChart data={this.state.domain1} options={chartOptions} />
//<DoughnutChart data={this.state.domain2} options={chartOptions} />
  render() {
    const profileName = this.props.name;
    const profileDetails = myData[profileName]
    return(
      <div>
        <Row>
          <Col sm={12}>
            <h1>{profileDetails.name}</h1>
          </Col>
        </Row>

        <Row>
          <Col sm={6}>
            <Thumbnail src={myProfileImage}>
              <h3>Profiles</h3>
              <p>
                <Button bsStyle="primary" onClick={this.routeToLinkedIn.bind(this)} >LinkedIn Profile</Button>&nbsp;
                <Button bsStyle="danger" onClick={this.routeToGitHub.bind(this)} >Github Profile</Button>
              </p>
            </Thumbnail>
            <br/>
          </Col>
          <Col sm={6}>
            <Jumbotron>
              <h2>{this.state.experiencedDomain}</h2>
              <p>{this.state.summary}</p>
              <h4>Proficient Skills</h4>
              <Col sm={6}>
                <Col sm={12}>
                  <ChartsComponent name={this.props.name} currentDomain="domain1"/>
                  
                </Col>
              </Col>
              <Col sm={6}>
                <Col sm={12}>
                  <ChartsComponent name={this.props.name} currentDomain="domain2"/>
                  
                </Col>
              </Col>              
            </Jumbotron>
          </Col>
          <Col sm={12}>
            <h2>Projects we are willing to take up</h2>
            <Col xs={6} md={4}>
              <Thumbnail src={myImage} alt="242x200">
                <h3>Thumbnail label</h3>
                <p>Description</p>
              </Thumbnail>
            </Col>
            <Col xs={6} md={4}>
              <Thumbnail src={myImage} alt="242x200">
                <h3>Thumbnail label</h3>
                <p>Description</p>
              </Thumbnail>
            </Col>
            <Col xs={12} md={4}>
              <Thumbnail src={myImage} alt="242x200">
                <h3>Thumbnail label</h3>
                <p>Description</p>
              </Thumbnail>
            </Col>
          </Col>
        </Row>
      </div>
    );
  }
}

export default MyProfile;